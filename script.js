const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];
  
  
  const root = document.querySelector('#root')
  const ul = document.createElement('ul')
 
   
  
  root.append(ul);
  
  
  
  
  function appendBooks(){
    
    books.forEach(book=>{
       try{
        if (!book.author){
            throw new Error(' No author in your object');
        }
        if (!book.name){
            throw new Error ('No name in your object')
        }
        if (!book.price){
            throw new Error('No price in your object')
        }  else{
            let li = document.createElement('li')
            li.style.cssText = "font-size: 20px"
            li.textContent = ` Автор книги -  ${book.author}, Назва -  ${book.name}, Ціна - ${book.price} `;
            ul.append(li);

        }
     }
        
        catch(e){
            console.error(e);
        }
    })
  }


appendBooks()